# SHipHawk ATM API

## Notes
While working on this test task, in order to reduce time spent on task,
I decided to not use any type of authentication
So, ATM is used by the only one user

## Installation
1. `git clone git@bitbucket.org:sumskyi/shiphawk_atm_api.git`
2. `cp config/database.yml.example config/database.yml`
3. edit `config/database.yml`
4. `rake db:create`
5. `rake db:migrate`

## Rake tasks
* print routes: `rake grape:routes`

## Running Web Server
`bundle exec rackup --port PORT --env ENVIRONMENT`  (default: development)

## Running tests
1. `APP_ENV=test rake db:migrate`
2. `bundle exec rspec`
