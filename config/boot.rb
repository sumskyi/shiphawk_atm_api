# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
require 'grape'
require 'otr-activerecord'

require_relative '../app/apis/atm'

OTR::ActiveRecord.configure_from_file! 'config/database.yml'
