# frozen_string_literal: true

require_relative '../models/note'

class WithdrawService
  ZeroBalance = Class.new(StandardError)
  MissingNotes = Class.new(StandardError)

  def self.call(amount)
    raise ZeroBalance, 'no money in ATM' if Note.total < amount

    tr_result = Note.transaction do
      result = Note.order(denomination: :desc).each_with_object({}) do |note, acc|
        next if note.denomination > amount

        to_get = [amount / note.denomination, note.amount].min

        amount -= note.denomination * to_get
        note.decrement!(:amount, to_get)

        acc[note.denomination] = to_get
      end

      raise ActiveRecord::Rollback, 'Missing notes rollback!' if amount > 0

      result
    end

    raise MissingNotes, 'Missing notes!' unless tr_result
  end
end
