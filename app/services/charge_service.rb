# frozen_string_literal: true

class ChargeService
  def self.call(params)
    notes = params[:notes] || params['notes']
    notes.each { |pair| charge(pair) }
  end

  private

  def self.charge(pair)
    denomination = pair[:denomination] || pair['denomination']
    note = Note.where(denomination: denomination).take

    if note
      note.increment!(:amount, pair[:amount])
    else
      Note.create!(pair)
    end
  end
end
