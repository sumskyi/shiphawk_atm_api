# frozen_string_literal: true

require_relative '../services/charge_service'
require_relative '../services/withdraw_service'

require_relative '../validators/amount_validator'

module ATM
  class API < Grape::API
    version 'v1', using: :path
    format :json
    rescue_from :all

    helpers do
    end

    desc 'charge ATM'
    params do
      requires :notes, type: Array do
        requires :denomination, type: Integer, values: [1, 2, 5, 10, 25, 50]
        requires :amount, type: Integer, amount: true
      end
    end
    post :charge do
      ChargeService.call(params)
    end

    desc 'withdraw money from ATM'
    params do
      requires :amount, type: Integer, amount: true
    end
    post :withdraw do
      WithdrawService.call(params)
    end
  end
end
