# frozen_string_literal: true

class Note < ActiveRecord::Base
  validates :denomination,
            inclusion: { in: [1, 2, 5, 10, 25, 50] },
            uniqueness: true
  validates :amount,
            numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  def self.total
    Note.all.inject(0) do |acc, row|
      acc += row[:denomination] * row[:amount]

      acc
    end
  end

  def total
    denomination * amount
  end
end
