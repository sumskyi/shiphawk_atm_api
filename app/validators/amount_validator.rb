# frozen_string_literal: true

class Amount < Grape::Validations::Base
  def validate_param!(attr_name, params)
    if params[:amount] && params[:amount] < 1
      raise Grape::Exceptions::Validation,
            params: [@scope.full_name(attr_name)],
            message: 'must be positive'
    end
  end
end
