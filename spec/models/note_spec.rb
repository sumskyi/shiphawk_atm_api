# frozen_string_literal: true

require_relative '../../app/models/note'

describe Note do
  describe 'denomination' do
    let(:params) do
      {
        denomination: -2,
        amount: 3
      }
    end

    it 'validates allowed value' do
      expect { Note.create!(params) }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  describe 'amount' do
    let(:params) do
      {
        denomination: 2,
        amount: -1
      }
    end

    it 'validates allowed value' do
      expect { Note.create!(params) }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
