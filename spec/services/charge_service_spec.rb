# frozen_string_literal: true

require_relative '../../app/services/charge_service'

describe ChargeService do
  subject(:result) { described_class.call(params) }

  let(:params) do
    { notes: [{ denomination: 2, amount: 1 }] }
  end

  context 'empty notes table' do
    it 'creates new record' do
      result

      expect(Note.count).to eql 1
    end
  end

  context 'existing records' do
    before do
      Note.create!(denomination: 2, amount: 0)
    end

    it 'increases denomination amount' do
      result

      expect(Note.where(denomination: 2).take.amount).to eql 1
    end
  end
end
