# frozen_string_literal: true

require_relative '../../app/services/withdraw_service'

describe WithdrawService do
  subject(:result) { described_class.call(amount) }

  context 'no money' do
    let(:amount) { 50 }

    it 'displays error' do
      expect { result }.to raise_error(WithdrawService::ZeroBalance)
    end
  end

  context 'money on file, right notes' do
    before do
      { 50 => 2, 25 => 8 }.each do |denomination, amount|
        Note.create!(denomination: denomination, amount: amount)
      end
    end

    context 'requested too much' do
      let(:amount) { 100_500 }

      it 'displays error if not enough' do
        expect { result }.to raise_error(WithdrawService::ZeroBalance)
      end
    end

    context 'no such notes' do
      let(:amount) { 51 }

      it 'displays error' do
        expect { result }.to raise_error(WithdrawService::MissingNotes)
      end

      it 'not changes notes in ATM' do
        expect { result }.to_not change { Note.total }
      rescue StandardError
      end
    end

    context 'notes ok' do
      let(:amount) { 175 }

      it 'processes' do
        expect { result }.to change { Note.total }.from(300).to(125)
      end
    end
  end
end
