# frozen_string_literal: true

class CreateNotes < ActiveRecord::Migration[5.2]
  def change
    create_table :notes do |t|
      t.integer :denomination
      t.integer :amount
    end

    add_index :notes, :denomination, unique: true
  end
end
