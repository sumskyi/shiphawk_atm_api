# frozen_string_literal: true

require './config/boot'

use OTR::ActiveRecord::ConnectionManagement
run ATM::API
